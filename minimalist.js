(function(){
    document.querySelector("#header").classList.add("favicon-current-background-color")

    var updateSelectedMOCallback = function(records){
        for(var i = 0; i < records.length; ++i) {
            var record = records[i];
            if(record.target.classList.contains("tab")) {
                if (record.target.classList.contains("active")) {
                    record.target.classList.remove("favicon-current-background-color")
                } else {
                    record.target.classList.add("favicon-current-background-color")
                }
            }
        }
    }

    var updateAllMOCallback = function(records) {
        var tabs = document.querySelectorAll(".tab");
        var fakerecords = []
        for(var i = 0; i < tabs.length; ++i){
            fakerecords.push({target:tabs[i]})
        }
        updateSelectedMOCallback(fakerecords)

        var maxWidth = 0
        var extwrappers = document.querySelectorAll("#main > .toolbar.toolbar-addressbar > .extensions-wrapper")
        for (var i = 0; i < extwrappers.length; ++i) {
            var canwidth = extwrappers[i].clientWidth
            if (canwidth > maxWidth) maxWidth = canwidth;
        }
        document.querySelector("#tabs-container").style.cssText = "width: calc(55% - "+maxWidth+"px) !important; margin-right: "+maxWidth+"px !important;"
    }

    // This one updates when a selection changes
    var moselected = new MutationObserver(updateSelectedMOCallback)
    // This one updates when a tab is created/removed
    var moall = new MutationObserver(updateAllMOCallback)

    setTimeout(function(){
        updateAllMOCallback()
        moselected.observe(document.querySelector(".tab-strip"),{subtree:true, attributes:true, attributeFilter:["class"]})
        moall.observe(document.querySelector(".tab-strip"),{childList:true})
    }, 1000)
})()
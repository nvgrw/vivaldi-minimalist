# vivaldi-minimalist
A modification to make Vivaldi's interface more minimalistic. It's very hacky, so use with caution.
I cannot take responsiblity for you breaking your Vivaldi installation.

Vivaldi is a browser that is very much still in development. Updates are likely to break this modification, requiring reinstallation or changes to the modification itself.
Furthermore it is important to note that I tested this modification using the Mac OS X version of Vivaldi. Because the Window decoration is somewhat different on other operating systems, I suspect that this will not work on those.

# Motivation
Browsers have cluttered UIs, and Vivaldi is certainly one of them. I removed buttons such as the bin and navigational buttons to free up space for the address bar. These buttons are redundant considering that there are keyboard shortcuts for these operations. I kept the reload button because it is a nice indicator for when a page has finished loading. The tab bar has been moved next to the address bar which, arguably, makes the browser look very much like what Microsoft have been doing... But even just the pixels wasted by the tab bar can have a drastic effect on the size of the website.

# Installation
1. Open Vivaldi
2. Navigate to the settings page and ensure that in appearance, "Use Page Theme Color in Interface" is enabled.
3. I'm not sure whether this has any effect, but you may need to switch to the ```Light``` theme if you are experiencing issues.
4. Head to the tabs section and make sure that the tabs position is set to ```Top```. Also disable "Transparent Background Tabs"
5. Navigate to ```/Applications/Vivaldi.app/Contents/Versions/VERSION NUMBER/Vivaldi Framework.framework/Resources/vivaldi```.
6. Modify ```browser.html``` to add ```<script src="minimalist.js"></script>``` below ```<script src="bundle.js"></script>```.
7. Modify ```style/common.css``` to add ```@import "minimalist.css";``` to a new line right at the top of the file.
8. Copy ```minimalist.js``` to the same directory that ```bundle.js``` resides in.
9. Copy ```minimalist.css``` to the same directory that ```common.css``` resides in.
10. Restart Vivaldi.

# Screenshots
![Reddit](screenshots/reddit.png?raw=true)
![New tab](screenshots/newtab.png?raw=true)

# License
See LICENSE file.
